<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>


<script type="text/javascript">
function logout() {
var answer = confirm ("you have successfully logged off , click on OK to continue.")
if (answer)
window.location="http://localhost:8080/loginHere";
}
</script>
<!-- javascript:AlertIt(); -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="newReminder?userId=${userId}">ADD</a></li>
			<li><a href="notes?userId=${userId}">Notes</a></li>
			<li><a href="profileEdit?userId=${userId}">Edit Profile</a></li>
		
		</ul >
		
		<h4 style="text-align: right"><a href="javascript:logout();" style ="color : white">Logout</a></h4>
		
	</div>
	</nav>
	
           <h3 ><center> Reminders</center></h3><hr>
	<div class="container">

		<table class="table">
			<thead>
				<tr>
					
					<th>reminderDate</th>
					<th>subject</th>
					<th>description</th>
					
					<th>edit</th>
					<th>delete</th>
					
				</tr>
			</thead>
			<tbody>
				<c:forEach var="reminder" items="${allReminder}">
					<tr>
						
						<td>${reminder.reminderDate}</td>
						<td>${reminder.subject}</td>
						<td>${reminder.description}</td>
						
						<td><a href="updateReminder?id=${reminder.id} "><div class="glyphicon glyphicon-pencil"></div></a></td>
					    <td><a href="delete?id=${reminder.id}"  onclick="return confirm('Are you sure you want to delete this item?');"><div class="glyphicon glyphicon-trash"></div></a></td>
					<%-- <td>${reminder.user.id} </td> --%>
					</tr>

				</c:forEach>

			</tbody>
		</table>

	</div>
</body>
</html>