package org.aayushi.test.springboot.notes;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface NotesRepository extends JpaRepository<Notes, Long>{

	public List<Notes> findByUserId(Long id);
	@Modifying
	  @Transactional
	  @Query(value ="delete from notes where user_id= ?", nativeQuery = true)
	  public void deleteNotes(Long id);
}
