/**
 * 
 */
package org.aayushi.test.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author aayushi
 *
 */
@SpringBootApplication
@EnableScheduling
public class TestSpringbootApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(TestSpringbootApplication.class, args);
		
		
		
		/*Reminder reminder = new Reminder();
		reminder.setDescription("test desc");
		reminder.setEmailSent(false);
		reminder.setId(1);
		reminder.setReminderDate(new Date());
		reminder.setSubject("Test subject");
		
		System.out.println(new ObjectMapper().writeValueAsString(reminder));*/
	}

}
